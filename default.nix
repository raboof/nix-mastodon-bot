{
  nixpkgs ? <nixpkgs>,
  config-edn,
  credentials-edn
}:

let
  pkgs = import nixpkgs {};
  mastodon-bot-slim = pkgs.runCommand "mastodon-bot-slim" {} ''
    cp -r ${pkgs.mastodon-bot} $out
    # Only here to get a version with
    # https://gitlab.com/yogthos/mastodon-bot/-/merge_requests/21 and
    # https://gitlab.com/yogthos/mastodon-bot/-/merge_requests/22 applied
    chmod ug+w $out/lib/node_modules/mastodon-bot
    chmod ug+w $out/lib/node_modules/mastodon-bot/*
    #rm $out/lib/node_modules/mastodon-bot/mastodon-bot.js
    SHEBANG=$(head -1 $out/lib/node_modules/mastodon-bot/mastodon-bot.js)
    echo $SHEBANG > $out/lib/node_modules/mastodon-bot/mastodon-bot.js
    tail -n +2 ${./mastodon-bot.js} >> $out/lib/node_modules/mastodon-bot/mastodon-bot.js
    #find $out -type f -print0 | while IFS= read -r -d $'\0' f; do substituteInPlace $f --replace "${pkgs.nodejs}" "${pkgs.nodejs-slim}"; done
  '';
  initScript = pkgs.writeScript "init" ''
    #!${pkgs.stdenv.shell}

    ${pkgs.dhcp.override {
      # apparently dhclient is now EOL, still need to look
      # into alternatives like dhcpcd or perhaps cloud-init?
      # possibly `dhcpcd --waitip 4 eth0`
      withClient = true;
    }}/bin/dhclient -v eth0

    export MASTODON_BOT_CONFIG=${config-edn}
    export MASTODON_BOT_CREDENTIALS=${credentials-edn}
    exec ${mastodon-bot-slim}/bin/mastodon-bot
  '';
  lib = pkgs.lib;
in lib.mkForce (pkgs.callPackage (nixpkgs + "/nixos/lib/make-system-tarball.nix") {
  fileName = "mastodon-bot-${builtins.baseNameOf config-edn}";
  storeContents = [
    {
      object = initScript;
      symlink = "/sbin/init";
    }
  ];
  contents = [
    { source = initScript; target = "/init"; }
  ];
  extraArgs = "--owner=0";
  extraCommands = "mkdir -p proc sys dev etc var/db";
})
